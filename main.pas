
Program HelloWorld;

Var 
  message: string;

Begin
  message := 'Hello world!';
  writeLn(message);
End.
