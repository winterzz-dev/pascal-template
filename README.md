# Pascal Template

This is a simple template for developing a Pascal application.

Tested on macOS, but should work on other systems as well.

## Requirements

For full functionality you need to install [Free Pascal Compiler](https://www.freepascal.org/download.html), [Taskfile](https://taskfile.dev/) to run build and run scripts and [VS Code](https://code.visualstudio.com/) to work with code.

VS Code will offer to install extensions to support the language on its own.

## Installation

Install with git clone

```shell
  git clone git@gitlab.com:winterzz-dev/pascal-template.git
  cd pascal-template
```

Next, you need to set the path to the PTOP formatter to a `.vscode/settings.json` file. The formatter is part of Free Pascal Compiler, so you don't need to install it separately. The path to the formatter can be obtained with the command `where ptop`.

## Usage/Examples

Template support build, run and run-watch scripts

```shell
task build -- main.pas
task run -- main.pas
task run-watch -- main.pas
```

## License

[MIT](https://choosealicense.com/licenses/mit/)
